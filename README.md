# GE-Lab OTEL

- [GE-Lab-OTEL](#ge-lab-OTEL)
  * [Motivação](#motivação)
  * [Laboratório](#laboratório)
  * [Documentação](#documentação)

## Motivação

Este é um Laboratório da Pós-Graduação da Fullcycle: Go Expert, Desenvolvimento Avançado em Golang

## Laboratório

**Objetivo:**
Desenvolver um sistema em Go que receba um CEP, identifica a cidade e retorna o clima atual (temperatura em graus celsius, fahrenheit e kelvin) juntamente com a cidade. Esse sistema deverá implementar OTEL(Open Telemetry) e Zipkin.

Basedo no cenário conhecido "Sistema de temperatura por CEP" denominado Serviço B, será incluso um novo projeto, denominado Serviço A.

**Requisitos - Serviço A (responsável pelo input):**

- O sistema deve receber um input de 8 dígitos via POST, através do schema:  { "cep": "29902555" }
- O sistema deve validar se o input é valido (contem 8 dígitos) e é uma STRING
	- Caso seja válido, será encaminhado para o Serviço B via HTTP
	- Caso não seja válido, deve retornar:
		- Código HTTP: 422
		- Mensagem: invalid zipcode

**Requisitos - Serviço B (responsável pela orquestração):**

- O sistema deve receber um CEP válido de 8 digitos
- O sistema deve realizar a pesquisa do CEP e encontrar o nome da localização, a partir disso, deverá retornar as temperaturas e formata-lás em: Celsius, Fahrenheit, Kelvin juntamente com o nome da localização.
- O sistema deve responder adequadamente nos seguintes cenários:
	- Em caso de sucesso:
		- Código HTTP: 200
		- Response Body: { "city: "São Paulo", "temp_C": 28.5, "temp_F": 28.5, "temp_K": 28.5 }
	 - Em caso de falha, caso o CEP não seja válido (com formato correto):
		- Código HTTP: 422
		- Mensagem: invalid zipcode
	- Em caso de falha, caso o CEP não seja encontrado:
		- Código HTTP: 404
		- Mensagem: **can not find zipcode**

Após a implementação dos serviços, adicione a implementação do OTEL + Zipkin:

- Implementar tracing distribuído entre Serviço A - Serviço B
- Utilizar span para medir o tempo de resposta do serviço de busca de CEP e busca de temperatura

**Dicas:**

- Utilize a API viaCEP (ou similar) para encontrar a localização que deseja consultar a temperatura: https://viacep.com.br/
- Utilize a API WeatherAPI (ou similar) para consultar as temperaturas desejadas: https://www.weatherapi.com/
- Para realizar a conversão de Celsius para Fahrenheit, utilize a seguinte fórmula: F = C * 1,8 + 32
- Para realizar a conversão de Celsius para Kelvin, utilize a seguinte fórmula: K = C + 273
	- Sendo F = Fahrenheit
	- Sendo C = Celsius
	- Sendo K = Kelvin
- Para dúvidas da implementação do OTEL, você pode clicar aqui
-  Para implementação de spans, você pode clicar aqui
- Você precisará utilizar um serviço de collector do OTEL
- Para mais informações sobre Zipkin, você pode clicar aqui

**Entrega:**

- O código-fonte completo da implementação.
- Documentação explicando como rodar o projeto em ambiente dev.
- Utilize docker/docker-compose para que possamos realizar os testes de sua aplicação.

## Documentação

Todos os testes foram realizados com go em sua versão 1.22.3 em arquitetura amd64.

A versão docker utilizada realização do laboratório e teste segue:

```
Client: Docker Engine - Community
 Version:           26.1.1
Server: Docker Engine - Community
 Engine:
  Version:          26.1.1
 containerd:
  Version:          1.6.31
 runc:
  Version:          1.1.12
 docker-init:
  Version:          0.19.0
```

Para iniciar o Quickstart Docker Compose faça o clone do repositório e o acesse:

```Bash
git clone git@gitlab.com:thalesfaggiano/ge-lab-otel.git
cd ge-lab-otel
```

---
### Quickstart Docker Compose

Passo 1:

```Bash
docker compose up --build
```
Em outro terminal execute os próximos passos.

Passo 2:

```Bash
# Teste o servico A
curl -X POST -H "Content-Type: application/json" -d '{"cep":"29902555"}' http://localhost:8080/cep
```
Exemplo de testes com o servico A:

![ge-lab-otel-servico-a](/images/servico-a.jpg)

Passo 3:

```Bash
# Teste o servico B
curl -X GET "http://localhost:8081/clima?cep=29902555" -H "accept: application/json" 
```

Exemplo de testes com o servico B:

![ge-lab-otel-servico-b](/images/servico-b.jpg)


Passo 4:

```Bash
# Acesse no seu navegador o site: 
http://localhost:9411
```

Passo 5:

Você iniciará vendo o menu **Find a trace**. 

![ge-lab-otel-image0](/images/otel00.jpg)

Ao clicar no botão **Run Query** você verá os resultados dos registros com os campos **Root, Start time, Spans e Duration**. 

![ge-lab-otel-image1](/images/otel01.jpg)

Para mais informações clique no botão **SHOW**

![ge-lab-otel-image2](/images/otel02.jpg)

Passo 6:

A seguir clique no botão **Dependencies** e depois em **RUN QUERY**

![ge-lab-otel-image3](/images/otel03.jpg)

Você verá um mapa com o lastro dos traces. Ao selecionar um ponto você verá os "traced requests" e seus gráficos.

![ge-lab-otel-image4](/images/otel04.jpg)
